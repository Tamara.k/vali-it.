package Voi;


public  class Suusataja {
    int stardiNr;
    double kiirus;
    double labitudDist;
    double dopinguKordaja; //ÜLESande -kui jookseb kiirem kui --siis
    public Suusataja (int i){
        stardiNr = i;
        kiirus = Math.random()*20; //20 km/t
        labitudDist = 0;
    }
    public void suusata(){
        labitudDist += kiirus/(3600);
    }
    public String toString (){
        int dist = (int) (labitudDist);
        return  stardiNr+": " + dist;

    }

    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDist>=koguDistants;
    }
}
// Suusataja