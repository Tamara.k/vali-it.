import Voi.Suusataja;

import java.util.ArrayList;

public   class Võistlus {
    ArrayList <Suusataja>voistlejad;
    int koguDistants;

    public Võistlus() {
        System.out.println("Start");
        voistlejad = new ArrayList();
        koguDistants = 20;
        for (int i = 0; i < 50; i++) {
            int voistleja = i;
            voistlejad.add(new Suusataja(i));
        }
        Aeg();

    }

    public void Aeg()  {
        for (Suusataja s : voistlejad) {
            s.suusata();
            
           boolean lopetanud =  s.kasOnLopetanud (koguDistants);
           if(lopetanud){
               System.out.println("Votja on" +s);
               return;
           }
        }
        System.out.println(voistlejad);

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Aeg();

    }
}
