import nadalapaevad.Laupäev;
import nadalapaevad.Pyhapaev;
import nadalapaevad.Reede1;

public class Main {
    public static void main(String[] args) {
        System.out.println("hello world");

        if ( (false && true) || false){ ///!falce znachit  obratnoe
            //boolean (only true and false )
            System.out.println("tõene");
        } else {
            System.out.println("väär");
        }

        Reede1.koju();  //reede on klass, koju on meetod.
        //klassi ja meetodi loomise shortcut alt+enter
        Laupäev.peole();
        Pyhapaev.hommik();
        Pyhapaev paev = new Pyhapaev();
        paev.maga ();
        paev.hommik();
       // paev.uni ();   //see on protected ,

    }
}
