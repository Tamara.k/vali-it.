import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ///keskesk teemaks on array ehk massiiv
        int[] massiiv = new int[6]; ///see var //pikkus on 6 (6 asja mahub sisse)
        ArrayList list = new ArrayList();  ///või see //list -- object -hetkel ei kasuta
        System.out.println(massiiv[2]);
        Arrays.toString(massiiv);
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);

        //Ül:määrata kolmanda nr 5. muuda 3 number 5ks
        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv)); //prindib terve massiv
        //: Prindi välja  kuues element massiivist
        //aga enne määra talle ka väärtust
        massiiv[5] = 13;
        System.out.println(massiiv[5]);
        //prind väla viimane el ükskõik kui pikk massiiv on)
        int viimane = massiiv[massiiv.length - 1];
        System.out.println("viimane " + viimane);
        /// loo uus massiiv , kus on kõik 8 numbrit kohe algeseks määratud
        int[] massiiv2 = new int[]{1, 2, 3, 4, 55, 6, 7, 8};
        System.out.println(Arrays.toString(massiiv2));
        //prindi välja ükshaaval kõik väärtused massiiv2 st
        int index = 0;
        while (index < massiiv2.length) {
            System.out.println(massiiv2[index]);
            index++; //index ++ = index +1


        }

        ///Teeme selle sama tsükli kiiremini kasutades for tsüklid
        for (int i = 0; i < massiiv2.length; i++) {
            System.out.println((massiiv2[i]));
            /// loo Stringide massiiv, mis on alguses tühi aga siis lisad keskele sõne

            String[] massiiv3 = new String[3];
            massiiv3[1] = "SUV";
            System.out.print(Arrays.toString(massiiv3));


//loo massiiv kus on 100 kohta, sisesto loetelu numbreid 0-99
///prindi välja
        }


        int[] mas4 = new int[100];
        for (int i = 0; i < mas4.length; i++) {
            mas4[10] = 5;
            mas4 [i] = i;
            System.out.println(i);
        }

        System.out.println(Arrays.toString(mas4)); // funkcija ostatka
        int mitupaarisarv = 0;
        for (int i = 0; i < mas4.length; i++){
            int a = mas4[i];
            if (a%2==0){
                mitupaarisarv = mitupaarisarv +1;
                System.out.println(i);
            }

        }

        //loo Arraylist ja sisesta sina 3 nr ja kolm sõna.
        ///1. sama massiv  massivi kus on numbrite jaga
        //loe mitu paarisarvu on //kasutada tuleb ni ctsüklid ka if laused
        //prindi tulemus{
        //kui jagad 2 arvu siis se tagastab jäägi
        // x 72 tähendab et o puhul on paarisarv ja 1 puhul ei ole
        //võyan 1 nr masstuuvist
        ArrayList list2 = new ArrayList();
        list2.add(4);
        list2.add(8);
        list2.add(954);
        list2.add("Krister");
        list2.add("peeter");

        // küsi viimasest List väla kolmas element ja prindi välja
        System.out.println (list2.get(2));
        System.out.println (list2);
        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list2.get(i));
            // code block to be executed
        }
        //prindi kogu list välja

// prindi iga element ükshaaval välja
Main.main(new String[1]);
        //loo uus array.list kus on 543 nr
        //kasuta see uus nr .  peavad olema math.random vahemikus 0-10
        //korruta iga nr  viiega
        // salvesta uus nr sma positsioonile
        for (int i = 0; i < mas4.length; i++){
            double nr = Math.random()*2;
            System.out.println(nr);
        }

    }


}