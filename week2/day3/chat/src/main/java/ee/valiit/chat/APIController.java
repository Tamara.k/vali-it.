
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import rooms.ChatMessage;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
@CrossOrigin
public class APIController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping("/chat/{room}")
    ArrayList chat(@PathVariable String room) { //SELECT*FROM mesages
        System.out.println("HERE");
        String sqlKask = "SELECT * FROM messages";
        ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
            String username = resultSet.getString("username");
            String message = resultSet.getString("message");
            return new ChatMessage(username, message);
        });
        return messages;
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
    }

}
