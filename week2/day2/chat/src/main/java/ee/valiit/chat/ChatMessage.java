package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.awt.*;
import java.util.Date;

public class ChatMessage {


    private String user;
    private String message;
    private String imageURL;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;

    public ChatMessage(){

    }


    public ChatMessage(String user, String message,String imageURL){
        this.user = user;
        this.message =message;
        this.date = new Date();
        this.imageURL = imageURL;

        //kerulisem  logisif arraylist random uuechatiruumiga (saadab mõlemad)
        //et chati 3 väli võib olla asukoht profiilipildi URLi

    }






    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getUser() {
        return user;
    }

    }

