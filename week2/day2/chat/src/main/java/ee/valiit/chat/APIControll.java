package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

@RestController  //ütleb et see klass on kontroller
@CrossOrigin  //lubab kõig tüübi päringud
public class APIControll {
    ChatRoom general  = new ChatRoom("general");
    @GetMapping ("/chat/general")
    ChatRoom Chat1(){
        return general;

    }
    @PostMapping ("/chat/general/new-message")
     void newMessage(@RequestBody ChatMessage msg ){
        general.addMessage(msg);//void sest ei tagasta midagi

    }

}
